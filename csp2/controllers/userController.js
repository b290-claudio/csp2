// used for encrypting password
const bcrypt = require("bcrypt");

// used for authentication
const auth = require("../auth");

// model used
const User = require("../models/User");


// Register
module.exports.registerUser = reqBody => {

	return User.find({ email : reqBody.email}).then(result => {
		if(result.length > 0){
			return false;
		} else {

			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				mobileNumber : reqBody.mobileNumber,
				password : bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then(user => true)
				.catch(err => {
					console.log(err);
					return false;
				});
		};
	}).catch(err => {
				console.log(err);
				return false;
		});
};


// Login
module.exports.loginUser = reqBody => {

	return User.findOne({ email : reqBody.email }).then(result => {

		// Check if email is registered or not
		if(result == null){

			console.log("Email not registered.")
			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// Password checker
			if(isPasswordCorrect){

				return { access : auth.createAccessToken(result) }

			} else {
				console.log("Incorrect password.");
				return false;
			}
		}
	}).catch(err => {

		console.log(err);

		return false
	});
};


// Check user details
module.exports.checkDetails = userInfo => {

	return User.findById(userInfo.userId).then(userResult => {
		userResult.password = "";
		return userResult;
	}).catch(err => {
		console.log(err);
		return false;
	});

};


// Set a User as Admin or Revoke Admin Rights
module.exports.setAdmin = (adminSetter, reqBody, adminEmail) => {
	return User.findById(adminSetter.userId).then(adminSet => {

		// Check if logged in user is the main Admin
		if(adminEmail.email === "admin@mail.com"){
			let settingAdmin = {
				isAdmin : reqBody.isAdmin
			}
			console.log(settingAdmin.isAdmin)
			if(settingAdmin.isAdmin === true){
				return User.findByIdAndUpdate(adminSetter.userId, settingAdmin).then(nowAdmin => {
					console.log(`User is now set as an admin.`)
					return true
				}).catch(err => {
					console.log(err);
					return false
				});
			} else {
				return User.findByIdAndUpdate(adminSetter.userId, settingAdmin).then(nowAdmin => {
					console.log(`User is revoked from being an admin.`)
					return true
				}).catch(err => {
					console.log(err);
					return false
				});
			}
			
		} else {
			console.log(`User is not the main Admin. Please email the main admin to request admin rights.`);
			return false;
		}
	}).catch(err => {
		// if ID param is invalid, this will display on the console
		console.log(`ID not found. Please check the user ID`);
		return false
	});
};


// Get all users and their information
module.exports.getUsers = () => {
	return User.find({}).then(result => result).catch(err => {
		console.log(err);
		return false;
	});
};


