// connect to models needed in this controller
const Product = require("../models/Product.js");
const User = require("../models/User.js");
const Order = require("../models/Order.js");


// controller for order checkout
module.exports.checkoutOrder = (data) => {
	return User.find({email : data.email}).then((check) => {
		return Product.findById(data.productId).then(order => {
			if(order.isActive === true && data.quantity > 0){
				let newOrder = new Order ({
					userId : data.userId,
					owner : `${data.firstName} ${data.lastName}`,
					products : [{
						productId : data.productId,
						productName : order.productName,
						quantity : data.quantity,
						unitPrice : order.price
					}],
					totalAmount : data.quantity*order.price
				});
				return newOrder.save().then(order => {
					console.log(`Product has been checkout.`)
					return true
				}).catch(err => {
					console.log(`Error in retrieving order`);
					return false;
				});
			} else {
				console.log(`${order.productName}
Sorry the product you are trying to checkout is either not available or invalid quantity.`)
				return false;
			}
		});
	}).catch(err => {
		console.log(err);
		return false;
	});
}


// Controller for cart
module.exports.cartCheckout = (data) => {
	let total = 0;
	return Order.find({userId : data.userId, isValid : true, isPaid : false}).then(resultOrder => {
		console.log(resultOrder)
		values = resultOrder.map(testing => {
			return testing.totalAmount + total
		})
		console.log(values)
		total = values.reduce((a,b) => a+b)
		console.log("Total: " + total)
		let paid = {
				isPaid : data.isPaid
			}
		return Order.updateMany({userId : data.userId, isPaid : false, isValid : true}, paid).then(done=>true).catch(err => {
			console.log(err);
			return false;
			});
	}).catch(err => {
		console.log(`All user's orders are paid`);
		return false;
	});
}


// Controller for Cart display (NON-ADMIN)
module.exports.myCart = (data) => {
	return Order.find({userId : data.userId, isPaid : false, isValid : true}).then(resultMyCart => resultMyCart).catch(err => {
		console.log(err);
		return false;
	});
}


// Controller for retrieving all orders of all users (ADMIN)
module.exports.allOrders = () => {
	return Order.find({}).then(resultAllOrders => resultAllOrders).catch(err => {
		console.log(err);
		return false;
	});
}


// Controller for retrieving all user orders (NON-ADMIN)
module.exports.myOrders = (data) => {
	return Order.find({userId : data.userId}).then(resultAllOrders => resultAllOrders).catch(err => {
		console.log(err);
		return false;
	});
}


// Controller for removing order (Non-Admin)
module.exports.removeOrder = (data, reqParams) => {
	return Order.findById({_id : reqParams.id}).then(removing => {
		if(removing.isPaid === false){
			if(removing.isValid === true){
				if(removing.userId === data.userId){
					let removed = {
						isValid : false
					}
					return Order.findByIdAndUpdate(removing.id, removed).then(done => {
						console.log(`Order is now cancelled. Please refresh the cart to update list.`);
						return true;
					}).catch(err => {
						console.log(err);
						return false;
					});
				} else {
					console.log(`You are not the owner of this order.`);
					return false;
				}
			} else {
				console.log(`This order is already cancelled.`);
				return false;
			}
		} else {
			console.log(`This order is already paid.`);
			return false;
		}
	}).catch(err => {
		console.log(err);
		return false;
	});
}


// Controller for re-adding order (Non-Admin)
module.exports.reAddOrder = (data, reqParams) => {
	return Order.findById({_id : reqParams.id}).then(reAdd => {
		if(reAdd.isPaid === false){
			if(reAdd.isValid === false){
				if(reAdd.userId === data.userId){
					let added = {
						isValid : true
					}
					return Order.findByIdAndUpdate(reAdd.id, added).then(done => {
						console.log(`Order is now re-added. Please refresh the cart to update list.`);
						return true;
					}).catch(err => {
						console.log(err);
						return false;
					});
				} else {
					console.log(`You are not the owner of this order.`);
					return false;
				}
			} else {
				console.log(`This order is already re-added.`);
				return false;
			}
		} else {
			console.log(`This order is already paid.`);
			return false;
		}
	}).catch(err => {
		console.log(err);
		return false;
	});
}


// Controller for changing quantities (non-admin)
module.exports.changeQuantity = (data, reqParams, reqBody) => {
	return Order.findById({_id : reqParams.id}).then(changing => {
		let searchProduct = changing.products.map(productId => {
			return productId.productId;
		})
		return Product.findById(searchProduct.toString()).then(price => {
			if(changing.isPaid === false){
				if(changing.isValid === true){
					if(changing.userId === data.userId){
						let changed = {
							products : [{
								productId : price.id,
								productName : price.productName,
								quantity : reqBody.quantity,
								unitPrice : price.price
							}],
							totalAmount : reqBody.quantity*price.price
						}
						return Order.findByIdAndUpdate(changing.id, changed).then(done => {
							console.log(`Order quantity was changed. Please refresh the cart to update list.`);
							return true;
						}).catch(err => {
							console.log(err);
							return false;
						});
					} else {
						console.log(`You are not the owner of this order.`);
						return false;
					}
				} else {
					console.log(`This order is already cancelled.`);
					return false;
				}
			} else {
				console.log(`This order is already paid.`);
				return false;
			}
		}).catch(err => {
			console.log(err);
			return false;
		});
	}).catch(err => {
		console.log(err);
		return false;
	});
}