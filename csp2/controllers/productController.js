// connect to product model
const Product = require("../models/Product");


// controller for adding product
module.exports.addProduct = (reqBody) => {

	return Product.find({productName : reqBody.productName}).then(checkExisting => {
		if(checkExisting.length > 0){
			console.log(`Product already existing. Check if it is the same product you are adding.`)
			return false
		} else {
			let newProduct = new Product ({
				productName : reqBody.productName,
				description : reqBody.description,
				price : reqBody.price
			});
			return newProduct.save().then(product => {
				console.log(`Product added.`);
				return true;
			}).catch(err => {
				console.log(`Error in adding product. Please check the details`);
				console.log(err);
				return false;
			});
		}
	}).catch(err => {
		console.log(err);
		return false;
	});
};


// controller for updating product
module.exports.updateProduct = (reqParams, reqBody) => {

	return Product.find({productName : reqBody.productName}).then(checkExistingProduct => {
		if(checkExistingProduct.length > 0){
			console.log(`Product name already existing. Check if it is the same product you are updating.`)
			return false
		} else {

			let updatedProduct = {
				productName : reqBody.productName,
				description : reqBody.description,
				price : reqBody.price
			};

			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
				.then(service => {
					console.log(`Product updated.`);
					return true;
				})
				.catch(err => {
					console.log(`Error in updating product.`);
					console.log(err);
					return false;
				});
		}
	}).catch(err => {
		console.log(err);
		return false;
	});
};


// controller for displaying specific product
module.exports.displayProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(display => {
		if(display.isActive === true){
			console.log(`Product Found`);
			return display;
		} else {
			console.log(`Product is not available.`);
			return false
		}
	}).catch(err => {
		console.log(`Can't find product.`);
		return false;
	});
};


// controller for displaying all products including inactive (for Admin only)
module.exports.displayProducts = () => {
	return Product.find({}).then(displayAll => displayAll)
		.catch(err => {
		console.log(err);
		return false;
	});
};


// controller for displaying all active products
module.exports.displayActive = () => {
	return Product.find({isActive : true}).then(activeResult => activeResult)
		.catch(err => {
		console.log(err);
		return false;
	});
};


// controller for archiving product
module.exports.archiveProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if(result.isActive === true){
			let archivedProduct = {
				isActive : false
			}
			return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then(archive => {
				console.log(`Product archived.`);
				return true;
			})
					.catch(err => {
						console.log(err);
						return false
					})
		} else {
			console.log(`Product is already archived.`);
			return false;
		}
	}).catch(err => {
		console.log(err);
		return false
	});
};


// controller for activating product
module.exports.activateProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if(result.isActive === false){
			let activatedProduct = {
				isActive : true
			}
			return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then(archive => {
				console.log(`Product is now active and visible to non-admin users.`)
				return true
			})
					.catch(err => {
						console.log(err);
						return false
					})
		} else {
			console.log(`Product is already activated.`);
			return false;
		}
	}).catch(err => {
		console.log(err);
		return false
	});
};


// CHANGE ALL RETURNED STRINGS INTO BOOLEAN FOR FULLSTACK