const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "User ID is required"]
	},
	owner : {
		type : String,
		required : [true, "Owner is required"]
	},
	products : [{
		productId : {
			type : String,
			required : [true, "Product ID is required"]
		},
		productName : {
			type : String,
			required : [true, "Product ID is required"]
		},
		quantity : {
			type : Number,
			required : [true, "Quantity is required"]
		},
		unitPrice : {
			type : Number,
			required : [true, "Unit price is required"]
		}
	}],
	totalAmount : {
		type : Number,
		required : [true, "Total amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	// checker if order is already paid so it is not included in cart
	isPaid : {
		type : Boolean,
		default : false
	},
	// this is to see if an order is cancelled or not
	isValid : {
		type : Boolean,
		default : true
	}
})

module.exports = mongoose.model("Order", orderSchema);