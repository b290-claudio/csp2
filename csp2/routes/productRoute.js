const express = require("express");
const router = express.Router();

// used for authentication
const auth = require("../auth.js");

// connect to controller
const productController = require("../controllers/productController.js");


// route for adding product
router.post("/newProduct", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === true){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
});


// route for updating product
router.patch("/:productId/updateProduct", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === true){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
});


// route for displaying specific product
router.get("/product/:productId", (req, res) => {
	productController.displayProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// route for displaying all product including inactive product (for Admin only)
router.get("/all", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === true){
		productController.displayProducts().then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
});


// route for displaying all active products
router.get("/", (req, res) => {
	productController.displayActive().then(resultFromController => res.send(resultFromController));
});


// route for archiving product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
});


// route for activating product
router.patch("/:productId/activate", auth.verify, (req, res) => {
	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === true){
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
});


module.exports = router;


// CHANGE ALL RETURNED STRINGS INTO BOOLEAN FOR FULLSTACK