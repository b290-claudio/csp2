const express = require("express");
const router = express.Router();

// used for authentication
const auth = require("../auth.js");

// connect to respective controller
const userController = require("../controllers/userController.js");



// regiter
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// login user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// retrieve user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.checkDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// set a user to admin (ADMIN use only)
router.patch("/admin/:userId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		userController.setAdmin(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
});


// retrieve all users (ADMIN)
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		userController.getUsers().then(resultFromController => res.send(resultFromController));
	} else {
		console.log(`User not Authorized.`);
		res.send(`User not Authorized`);
	}
})


module.exports = router;