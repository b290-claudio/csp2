const express = require("express");
const router = express.Router();


// used for authentication
const auth = require("../auth.js");

// connect to controller
const orderController = require("../controllers/orderController.js");


// route for order checkout
router.post("/checkout", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id,
		firstName : userData.firstName,
		lastName : userData.lastName,
		email : userData.email,
		productId : req.body.productId,
		quantity : req.body.quantity
		};
		console.log(userData.firstName)
		orderController.checkoutOrder(data).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
});



// route for cart + Paying
router.patch("/cart/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id,
		isPaid : req.body.isPaid
	};

		orderController.cartCheckout(data).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
})



// route for displaying cart
router.get("/cart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id
	};

		orderController.myCart(data).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
})


// route for retrieving all orders (ADMIN)
router.get("/allOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === true){
		let data = {
		userId : userData.id
	};

		orderController.allOrders(data).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is not an Admin.`);
		res.send(false);
	}
})


// route for retrieving all user orders (Non-admin)
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id
	};

		orderController.myOrders(data).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
})


// route for removing orders
router.patch("/deleteOrder/:id", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id
		};

		orderController.removeOrder(data, req.params).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
});

// route for readding cancelled order(s)
router.patch("/reAddOrder/:id", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id
		};

		orderController.reAddOrder(data, req.params).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
});



// route for changing order quantity
router.patch("/:id/changeQuantity", auth.verify, (req, res) => {

	// check if user is registered
	const userData = auth.decode(req.headers.authorization);

	// check if user is authorized to add new products
	if(userData.isAdmin === false){
		let data = {
		userId : userData.id
		};

		orderController.changeQuantity(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
	
	} else {
		console.log(`User is an Admin.`);
		res.send(false);
	}
});



module.exports = router;