const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");


/* Routes Access */
const userRoute = require("./routes/userRoute.js");
const productRoute = require("./routes/productRoute.js");
const orderRoute = require("./routes/orderRoute.js");





/* Create Server */
const app = express();

/* Connect to MongoDB */
mongoose.connect("mongodb+srv://admin:admin123@zuitt.njjittd.mongodb.net/MiraiClaud-DB?retryWrites=true&w=majority", {
		useNewUrlParser : true,
		useUnifiedTopology : true
});


/* Check database connection */
mongoose.connection.on("error", console.error.bind(console, `Connection Error!`));
mongoose.connection.once("open", () => console.log(`You are now connected to MongoDB Atlas.`));

/* Middlewares */
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));


/* Routes */
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);




/* Listen and Export */
if(require.main === module){
	app.listen(process.env.PORT || 8080, () => {
		console.log(`API Online Port ${process.env.PORT || 8080}`)
	});
}

module.exports = {app, mongoose};